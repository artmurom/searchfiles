package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.model.FileBrowserModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;


public class StatusBarPanel implements Observer {

    private static final Insets FINAL_INSETS =
            new Insets(5, 5, 5, 5);

    private FileBrowserModel model;

    private SearchFileFrame frame;
    private JPanel panel;
    private JProgressBar progressBar;
    private JLabel label;

    private String strFinishSearch = "Поиск завершен. Найдено %1$d файлов за %2$d секунд", strSearch = "Поиск файлов";

    public StatusBarPanel(SearchFileFrame frame, FileBrowserModel model) {
        this.frame = frame;
        this.model = model;
        createPartControl();
    }

    private void createPartControl() {
        model.addObserver(this);

        panel = new JPanel(new FlowLayout());
        panel.setLayout(new BorderLayout());
        panel.setBorder(new EmptyBorder(FINAL_INSETS));

        label = new JLabel();

        progressBar = new JProgressBar();
        progressBar.setVisible(false);

        panel.add(label, BorderLayout.WEST);
        panel.add(progressBar, BorderLayout.EAST);
        panel.setVisible(false);

    }

    public void startProgressBar() {
        label.setText(strSearch);
        panel.setVisible(true);
        progressBar.setVisible(true);
        progressBar.setIndeterminate(true);
    }

    public void stopProgressBar() {
        label.setText(String.format(strFinishSearch, model.getCountFiles(), model.getTimeUpdate()));
        progressBar.setVisible(false);
        progressBar.setIndeterminate(false);
    }

    public JPanel getPanel() {
        return panel;
    }


    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof FileBrowserModel) {
            if (o.equals("start update")) {
                startProgressBar();
            } else {
                stopProgressBar();
            }
        }
    }
}