package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.controller.TreeController;
import ru.artemtsarev.search_files.model.FileBrowserModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;


public class TreeScrollPane implements Observer {

    private TreeController controller;

    private FileBrowserModel model;

    private SearchFileFrame frame;
    private JScrollPane scrollPane;
    private JTree tree;


    public TreeScrollPane(SearchFileFrame frame,
                          FileBrowserModel model) {
        this.frame = frame;
        this.model = model;
        createPartControl();
    }

    private void createPartControl() {
        model.addObserver(this);

        tree = new JTree();
        tree.setRootVisible(false);
        tree.setVisible(false);


        scrollPane = new JScrollPane(tree);

        Dimension preferredSize = scrollPane.getPreferredSize();
        Dimension widePreferred = new Dimension(
                300, (int) preferredSize.getHeight());
        scrollPane.setPreferredSize(widePreferred);
    }

    public void updateTree() {
        tree = new JTree(model.getModel());

        controller = new TreeController(this, model);

        tree.setVisible(true);
        tree.setRootVisible(false);
        scrollPane.setViewportView(tree);
        scrollPane.repaint();
    }

    public void addMouseListener(MouseListener listenForTree) {
        tree.addMouseListener(listenForTree);
    }

    public JTree getTree() {
        return tree;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public SearchFileFrame getParent() {
        return frame;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof FileBrowserModel) {
            if (o.equals("start update")) {
            } else {
                updateTree();
            }
        }
    }
}