package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.controller.TabController;
import ru.artemtsarev.search_files.model.FileTab;
import ru.artemtsarev.search_files.view.utills.Utills;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


public class Tab implements Observer {

    private static final Insets FINAL_INSETS =
            new Insets(5, 5, 5, 5);

    private TabController controller;

    private JTabbedPane tabPane;
    private JButton btnSelectAll, btnPrevious, btnNext, btnClose;
    private JLabel lblTitle;
    private JTextArea textArea;

    public Tab(JTabbedPane tabPane, FileTab fileTab) {
        this.tabPane = tabPane;
        addTab(fileTab);
        controller = new TabController(this, fileTab);
        fileTab.addObserver(this);
    }

    public void addTab(FileTab fileTab) {

        String fileName = fileTab.getFileName();

        if (tabPane.indexOfTab(fileName) < 0) {

            JPanel panelMain = new JPanel(new BorderLayout());

            JPanel panelBottom = new JPanel();

            btnSelectAll = new JButton();
            btnSelectAll.setIcon(Utills.resizeImage("src/res/image/select_all.png", 16, 16));
            panelBottom.add(btnSelectAll);

            btnPrevious = new JButton();
            btnPrevious.setIcon(Utills.resizeImage("src/res/image/previous.png", 16, 16));
            btnPrevious.setEnabled(false);
            panelBottom.add(btnPrevious);

            btnNext = new JButton();
            btnNext.setIcon(Utills.resizeImage("src/res/image/next.png", 16, 16));
            panelBottom.add(btnNext);

            textArea = new JTextArea();
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            Font f = new Font("Serif", Font.PLAIN, 14);
            textArea.setFont(f);

            updateTextArea(fileTab);

            JScrollPane scrollPane = new JScrollPane(textArea);

            panelMain.add(scrollPane, BorderLayout.CENTER);
            panelMain.add(panelBottom, BorderLayout.SOUTH);

            tabPane.addTab(fileName, panelMain);
            int index = tabPane.indexOfTab(fileName);
            JPanel pnlTab = new JPanel(new GridBagLayout());
            pnlTab.setOpaque(false);
            lblTitle = new JLabel(fileName);
            btnClose = new JButton();
            btnClose.setBorder(new EmptyBorder(FINAL_INSETS));
            btnClose.setOpaque(false);
            btnClose.setContentAreaFilled(false);
            btnClose.setBorderPainted(false);
            btnClose.setIcon(Utills.resizeImage("src/res/image/close.png", 14, 14));

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1;

            pnlTab.add(lblTitle, gbc);

            gbc.gridx++;
            gbc.weightx = 0;
            pnlTab.add(btnClose, gbc);

            tabPane.setTabComponentAt(index, pnlTab);
        }

        tabPane.setSelectedIndex(tabPane.indexOfTab(fileName));
    }

    public void updateTextArea(FileTab fileTab) {

        btnPrevious.setEnabled(true);

        if (fileTab.getPosition() == 0) {
            btnPrevious.setEnabled(false);
        }
        ArrayList<String> strings = (ArrayList) fileTab.getPage();
        if (strings.size() == 0) {
            btnNext.setEnabled(false);
            textArea.setText("");
        } else {
            if (strings.size() < 50) {
                btnNext.setEnabled(false);
            } else {
                btnNext.setEnabled(true);
            }
            textArea.setText("");

            for (String s : strings) {
                textArea.append(s + "\n");
            }

            initHighLight(fileTab);
        }
        textArea.setCaretPosition(0);
    }

    private void initHighLight(FileTab fileTab) {

        Highlighter highlighter = textArea.getHighlighter();
        Highlighter.HighlightPainter painter =
                new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);

        String strSearch = fileTab.getStrSearh();

        if (!strSearch.isEmpty()) {
            String[] strings1 = textArea.getText().split("\n");
            int position = 0;

            for (int i = 0; i < strings1.length; i++) {
                String string = strings1[i];
                if (string.contains(strSearch)) {
                    highlightString(string, strSearch, position, highlighter, painter);
                }
                position += string.length() + 1;
            }
        }
    }

    private void highlightString(String line, String highlight, int position, Highlighter highlighter, Highlighter.HighlightPainter painter) {
        int start = line.indexOf(highlight) + position;
        int finish = start + highlight.length();
        try {
            highlighter.addHighlight(start, finish, painter);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        String subString = line.substring(line.indexOf(highlight) + highlight.length(), line.length());
        if (subString.length() > 0 && subString.contains(highlight)) {
            highlightString(subString, highlight, finish, highlighter, painter);
        }
    }

    public void closeTab(String fileName) {
        int index = tabPane.indexOfTab(fileName);
        if (index >= 0) {
            tabPane.removeTabAt(index);
        }
    }

    public void selectAll() {
        textArea.selectAll();
        textArea.requestFocus();
    }

    public void addSelectAllListener(ActionListener selectAllListener) {
        btnSelectAll.addActionListener(selectAllListener);
    }

    public void addNextPageListener(ActionListener nextPageListener) {
        btnNext.addActionListener(nextPageListener);
    }

    public void addPreviousPageListener(ActionListener previousPageListener) {
        btnPrevious.addActionListener(previousPageListener);
    }

    public void addCloseTabListener(ActionListener closeTabListener) {
        btnClose.addActionListener(closeTabListener);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof FileTab) {
            updateTextArea((FileTab) observable);
        }
    }
}