package ru.artemtsarev.search_files.view.utills;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Utills {

    public Utills() {
    }

    public static ImageIcon resizeImage(String path, int width, int height) {

        ImageIcon imageIcon = null;

        try {
            Image buttonIcon = ImageIO.read(new File(path));
            Image buttonIcon2 = buttonIcon.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            imageIcon = new ImageIcon(buttonIcon2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageIcon;
    }
}
