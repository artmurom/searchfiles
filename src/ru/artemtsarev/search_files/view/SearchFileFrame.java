package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.model.FileBrowserModel;
import ru.artemtsarev.search_files.model.FileTab;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SearchFileFrame {

    public static final String APP_NAME = "Поиск файлов";

    private FileBrowserModel model;

    private JFrame frame;
    private JPanel mainPanel;
    private TreeScrollPane treeScrollPane;
    private SearchPanel searchPanel;
    private StatusBarPanel statusBarPanel;
    private TabPane tabPane;

    public SearchFileFrame(FileBrowserModel model) {
        this.model = model;
        setLookAndFeel();
        createPartControl();
    }

    private void createPartControl() {
        frame = new JFrame();
        frame.setTitle(APP_NAME);
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.getImage("src/res/image/app_icon.png");
        frame.setIconImage(img);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(640, 480);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                exitProcedure();
            }
        });

        createMainPanel();

        frame.add(mainPanel);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    private void createMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        searchPanel = new SearchPanel(this, model);
        mainPanel.add(searchPanel.getPanel(), BorderLayout.NORTH);

        treeScrollPane = new TreeScrollPane(this, model);
        mainPanel.add(treeScrollPane.getScrollPane(), BorderLayout.WEST);

        statusBarPanel = new StatusBarPanel(this, model);
        mainPanel.add(statusBarPanel.getPanel(), BorderLayout.SOUTH);

        tabPane = new TabPane(this);
        mainPanel.add(tabPane.getTabPane(), BorderLayout.CENTER);

    }

    public void exitProcedure() {
        frame.dispose();
        System.exit(0);
    }

    public void addTabPane(FileTab fileTab) {
        tabPane.addTab(fileTab);
    }

    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception weTried) {
            weTried.printStackTrace();
        }
    }
}
