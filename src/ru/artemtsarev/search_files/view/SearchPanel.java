package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.controller.SearchController;
import ru.artemtsarev.search_files.model.FileBrowserModel;
import ru.artemtsarev.search_files.view.utills.Utills;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;


public class SearchPanel implements Observer {

    private static final Insets FINAL_INSETS =
            new Insets(5, 5, 5, 5);

    private SearchController controller;

    private FileBrowserModel model;

    private SearchFileFrame frame;
    private JLabel labelEnterText, labelChooseFolder, labelChooseExtension;
    private JButton btnChooseFolder, btnSearch, btnInterruptSearch;
    private JTextField textFieldEnterText, textFieldFolder, textFieldExtension;
    private JPanel panel, panelFolder;
    private JFileChooser chooser;

    private String strLabelEnterText = "Введите строку поиска", strLabelChooseFolder = "Выберите директорию", strLabelChooseExtension = "Расширение", strBtnSearch = "Поиск", strBtnInterruptSearch = "Прервать", strBtnChooseFolder = "Указать";

    public SearchPanel(SearchFileFrame frame, FileBrowserModel model) {
        this.frame = frame;
        this.model = model;

        createPartControl();
    }

    private void createPartControl() {

        model.addObserver(this);

        panel = new JPanel(new GridBagLayout());
        panelFolder = new JPanel();
        panelFolder.setLayout(new BoxLayout(panelFolder, BoxLayout.X_AXIS));

        int gridy = 0;

        labelEnterText = new JLabel(strLabelEnterText);
        addComponent(panel, labelEnterText, 0, gridy, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        textFieldEnterText = new JTextField("", 40);
        addComponent(panel, textFieldEnterText, 1, gridy++, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        labelChooseFolder = new JLabel(strLabelChooseFolder);
        addComponent(panel, labelChooseFolder, 0, gridy, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        textFieldFolder = new JTextField("C://", 40);

        btnChooseFolder = new JButton();
        btnChooseFolder.setIcon(Utills.resizeImage("src/res/image/open_folder.png", 14, 14));

        panelFolder.add(textFieldFolder);
        panelFolder.add(btnChooseFolder);

        addComponent(panel, panelFolder, 1, gridy++, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);


        labelChooseExtension = new JLabel(strLabelChooseExtension);
        addComponent(panel, labelChooseExtension, 0, gridy, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        textFieldExtension = new JTextField(".log", 40);
        addComponent(panel, textFieldExtension, 1, gridy++, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        btnSearch = new JButton(strBtnSearch);

        addComponent(panel, btnSearch, 0, gridy, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        btnInterruptSearch = new JButton(strBtnInterruptSearch);
        btnInterruptSearch.setVisible(false);

        addComponent(panel, btnInterruptSearch, 0, gridy, 1, 1,
                FINAL_INSETS, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE);

        controller = new SearchController(this, model);

    }

    private void addComponent(Container container, Component component,
                              int gridx, int gridy, int gridwidth, int gridheight,
                              Insets insets, int anchor, int fill) {
        GridBagConstraints gbc = new GridBagConstraints(gridx, gridy,
                gridwidth, gridheight, 1.0D, 1.0D, anchor, fill,
                insets, 0, 0);
        container.add(component, gbc);
    }

    public String getText() {
        return textFieldEnterText.getText();
    }

    public String getFolder() {
        return textFieldFolder.getText();
    }

    public void setFolder(String path) {
        textFieldFolder.setText(path);
    }

    public String getExtension() {
        return textFieldExtension.getText();
    }

    public void showBtnInterruptSearch() {
        btnSearch.setVisible(false);
        btnInterruptSearch.setVisible(true);
    }

    public void showBtnSearch() {
        btnSearch.setVisible(true);
        btnInterruptSearch.setVisible(false);
    }

    public void showJFileChooser() {
        chooser = new JFileChooser();

        chooser.setCurrentDirectory(new File("C://"));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = chooser.showOpenDialog(panel);

        if (result == JFileChooser.APPROVE_OPTION) {
            String name = chooser.getSelectedFile().getAbsolutePath();
            setFolder(name);
        }
    }

    public void addSearchListener(ActionListener listenForSearchButton) {
        btnSearch.addActionListener(listenForSearchButton);
    }

    public void addInterruptSearchListener(ActionListener listenForInterruptSearchButton) {
        btnInterruptSearch.addActionListener(listenForInterruptSearchButton);
    }

    public void addJFileChooserListener(ActionListener listenForInterruptSearchButton) {
        btnChooseFolder.addActionListener(listenForInterruptSearchButton);
    }

    public JPanel getPanel() {
        return panel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof FileBrowserModel) {
            if (o.equals("start update")) {
                showBtnInterruptSearch();
            } else {
                showBtnSearch();
            }
        }
    }
}