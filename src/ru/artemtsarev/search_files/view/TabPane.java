package ru.artemtsarev.search_files.view;

import ru.artemtsarev.search_files.model.FileTab;

import javax.swing.*;
import java.awt.*;


public class TabPane {

    private SearchFileFrame frame;
    private JTabbedPane tabPane;

    public TabPane(SearchFileFrame frame) {
        this.frame = frame;
        createPartControl();
    }

    private void createPartControl() {
        tabPane = new JTabbedPane();

        Dimension preferredSize = tabPane.getPreferredSize();
        Dimension widePreferred = new Dimension(
                500, (int) preferredSize.getHeight());
        tabPane.setPreferredSize(widePreferred);
    }


    protected void addTab(FileTab fileTab) {
        Tab tab = new Tab(tabPane, fileTab);
    }

    public JTabbedPane getTabPane() {
        return tabPane;
    }

}