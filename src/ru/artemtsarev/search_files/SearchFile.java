package ru.artemtsarev.search_files;

import ru.artemtsarev.search_files.model.FileBrowserModel;
import ru.artemtsarev.search_files.view.SearchFileFrame;

import javax.swing.*;

public class SearchFile implements Runnable {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new SearchFile());
    }

    @Override
    public void run() {
        new SearchFileFrame(new FileBrowserModel());
    }
}
