package ru.artemtsarev.search_files.model;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.*;
import java.util.*;

public class FileBrowserModel extends Observable {

    private Date timeStartSearch, timeEndSearch;

    private DefaultTreeModel model;

    private ArrayList<File> files;

    private int countFiles = 0;

    private boolean interrupt = false;

    public void createTreeModel(String text, String path, String extension) {

        setChanged();
        notifyObservers("start update");

        files = new ArrayList<>();

        interrupt = false;

        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        model = new DefaultTreeModel(root);

        System.out.println("Start" + new Date().toString());

        timeStartSearch = new Date();


        walk(text, path, extension);

        for (File file : files) {
            buildTree(model, file, text);
        }

        countFiles = files.size();

        timeEndSearch = new Date();

        System.out.println("Finish" + new Date().toString());

        setChanged();
        notifyObservers("model update");
    }

    public void interruptCreateTreeModel() {
        interrupt = true;
    }

    public DefaultTreeModel getModel() {
        return model;
    }

    private void buildTree(final DefaultTreeModel model, final File file, final String text) {
        // Fetch the root node
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

        String str = file.getAbsolutePath();

        str = str.replace("\\\\", "");
        str = str.replace("\\", "/");


        // Split the string around the delimiter
        String[] strings = str.split("/");

        // Create a node object to use for traversing down the tree as it
        // is being created
        DefaultMutableTreeNode node = root;

        // Iterate of the string array
        for (int i = 0; i < strings.length; i++) {
            String s = strings[i];

            int index = childIndex(node, s);

            // Index less than 0, this is a new node not currently present on the tree
            if (index < 0) {
                // Add the new node
                DefaultMutableTreeNode newChild;
                if (i == strings.length - 1) {
                    newChild = new DefaultMutableTreeNode(new FileNode(new File(file.getAbsolutePath()), text));
                } else {
                    newChild = new DefaultMutableTreeNode(s);
                }
                node.insert(newChild, node.getChildCount());
                node = newChild;
            }
            // Else, existing node, skip to the next string
            else {
                node = (DefaultMutableTreeNode) node.getChildAt(index);
            }
        }
    }

    private int childIndex(final DefaultMutableTreeNode node, final String childValue) {
        Enumeration<DefaultMutableTreeNode> children = node.children();
        DefaultMutableTreeNode child = null;
        int index = -1;

        while (children.hasMoreElements() && index < 0) {
            child = children.nextElement();

            if (child.getUserObject() != null && childValue.equals(child.getUserObject())) {
                index = node.getIndex(child);
            }
        }

        return index;
    }

    private void walk(String text, String path, String extension) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) return;

        for (File f : list) {
            if (interrupt == true) {
                break;
            }
            if (f.isDirectory()) {
                walk(text, f.getAbsolutePath(), extension);
            } else {
                if (f.getAbsolutePath().endsWith(extension)) {
                    if (containsString(f, text)) {
                        files.add(f);
                    }
                }
            }
        }
    }

    private boolean containsString(File f, String str) {

        boolean result = false;

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f.getAbsolutePath())));

            String line;
            while ((line = br.readLine()) != null && !result) {
                if (interrupt == true) {
                    break;
                }
                result = line.contains(str);
            }
        } catch (IOException e) {
            System.err.println(f.getAbsolutePath());
        } catch (NullPointerException e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public long getTimeUpdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timeStartSearch);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(timeEndSearch);

        return (calendar1.getTimeInMillis() - calendar.getTimeInMillis()) / 1000;
    }

    public int getCountFiles() {
        return countFiles;
    }
}