package ru.artemtsarev.search_files.model;

import java.io.File;

public class FileNode {

    private File file;
    private String strSearch;

    public FileNode(File file, String strSearch) {
        this.file = file;
        this.strSearch = strSearch;
    }

    public File getFile() {
        return file;
    }

    public String getStrSearch() {
        return strSearch;
    }

    @Override
    public String toString() {
        String name = file.getName();
        if (name.equals("")) {
            return file.getAbsolutePath();
        } else {
            return name;
        }
    }

}
