package ru.artemtsarev.search_files.model;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileTab extends Observable {

    private static final int COUNT_READ_LINES = 50;

    private File file;

    private List<String> page, buffer, encoding = Arrays.asList("UTF-8", "Windows-1251", "ISO-8859-1", "US-ASCII");

    private String fileName;

    private String strSearh;

    private int position = 0, positionBuffer = 0, positionBufferList, numberEncoding = 0;

    private boolean increment = true, firstUpdate = true;

    public FileTab(File file, String strSearh) {
        this.file = file;
        this.strSearh = strSearh;
        fileName = file.getName();
        updateBuffer(encoding.get(numberEncoding));
    }

    public String getFileName() {
        return fileName;
    }

    public File getFile() {
        return file;

    }

    public String getStrSearh() {
        return strSearh;
    }

    public void readPage() {

        if (Math.abs(position - positionBuffer) >= 30) {
            updateBuffer(encoding.get(numberEncoding));
        }

        if (increment) {
            positionBufferList += 50;
            page = new ArrayList<>(buffer.subList(positionBufferList - 50, positionBufferList < buffer.size() ? positionBufferList : buffer.size()));
        } else {
            positionBufferList -= 50;
            page = new ArrayList<>(buffer.subList(positionBufferList - 50, positionBufferList));
        }

        setChanged();
        notifyObservers();
    }

    public List<String> getPage() {
        return page;
    }

    public void isIncrement(boolean increment) {
        this.increment = increment;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    private void updateBuffer(String charset) {

        int positionBufferTemp = positionBuffer;

        Thread thread = new Thread(() -> {

            try (Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()), Charset.forName(charset))) {

                positionBuffer = position;

                int firstElement;
                int limit;
                if (position < 60) {
                    firstElement = 0;
                    limit = 6000;
                } else {
                    firstElement = (position - 60) * COUNT_READ_LINES;
                    limit = 6000;
                }
                buffer = lines.skip(firstElement).limit(limit).collect(Collectors.toList());

                if (position == 0) {
                    positionBufferList = 0;
                } else if (position < 60) {
                    positionBufferList = (position + 1) * COUNT_READ_LINES;
                } else {
                    positionBufferList = 3050;
                }

                if (firstUpdate) {
                    readPage();
                    firstUpdate = false;
                }
            } catch (IOException e) {
                positionBuffer = positionBufferTemp;
            } catch (UncheckedIOException e) {
                numberEncoding += 1;
                updateBuffer(encoding.get(numberEncoding));
            }
        });
        thread.start();
    }
}
