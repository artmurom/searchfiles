package ru.artemtsarev.search_files.controller;

import ru.artemtsarev.search_files.model.FileBrowserModel;
import ru.artemtsarev.search_files.view.SearchPanel;

import java.awt.event.ActionListener;

public class SearchController {

    private SearchPanel view;

    private FileBrowserModel model;

    public SearchController(SearchPanel view, FileBrowserModel model) {
        this.view = view;
        this.model = model;

        view.addSearchListener(getSearchListener());
        view.addInterruptSearchListener(getInterruptSearchListener());
        view.addJFileChooserListener(getJFileChooserListener());
    }


    public ActionListener getSearchListener() {
        return actionEvent -> {
            Thread thread = new Thread(() -> {
                model.createTreeModel(view.getText(), view.getFolder(), view.getExtension());
            });
            thread.start();
        };
    }

    public ActionListener getInterruptSearchListener() {
        return actionEvent -> {
            model.interruptCreateTreeModel();
        };
    }

    public ActionListener getJFileChooserListener() {
        return actionEvent -> {
            view.showJFileChooser();
        };
    }

}

