package ru.artemtsarev.search_files.controller;

import ru.artemtsarev.search_files.model.FileBrowserModel;
import ru.artemtsarev.search_files.model.FileNode;
import ru.artemtsarev.search_files.model.FileTab;
import ru.artemtsarev.search_files.view.TreeScrollPane;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

public class TreeController {

    private TreeScrollPane view;

    private FileBrowserModel model;

    public TreeController(TreeScrollPane view, FileBrowserModel model) {
        this.view = view;
        this.model = model;

        view.addMouseListener(getMouseListener());
    }


    public MouseListener getMouseListener() {

        return new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    try {
                        TreePath treePath = view.getTree().getPathForLocation(mouseEvent.getX(), mouseEvent.getY());

                        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();

                        if (defaultMutableTreeNode.getChildCount() == 0) {
                            FileNode fileNode = (FileNode) defaultMutableTreeNode.getUserObject();
                            File file = fileNode.getFile();
                            String strSearch = fileNode.getStrSearch();
                            FileTab fileTab = new FileTab(file, strSearch);
                            view.getParent().addTabPane(fileTab);
                        }
                    } catch (NullPointerException e) {

                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }

        };


    }


}


