package ru.artemtsarev.search_files.controller;

import ru.artemtsarev.search_files.model.FileTab;
import ru.artemtsarev.search_files.view.Tab;

import java.awt.event.ActionListener;

public class TabController {

    private Tab view;

    private FileTab model;

    public TabController(Tab view, FileTab model) {
        this.view = view;
        this.model = model;

        view.addSelectAllListener(getSelectAllListener());
        view.addNextPageListener(getNextPageListener());
        view.addPreviousPageListener(getPreviousPageListener());
        view.addCloseTabListener(getCloseTabListener());
    }

    public ActionListener getSelectAllListener() {
        return actionEvent -> {
            view.selectAll();
        };
    }

    public ActionListener getNextPageListener() {
        return actionEvent -> {
            int position = model.getPosition() + 1;
            model.setPosition(position);
            model.isIncrement(true);
            model.readPage();
        };
    }

    public ActionListener getPreviousPageListener() {
        return actionEvent -> {
            int position = model.getPosition() - 1;
            model.setPosition(position);
            model.isIncrement(false);
            model.readPage();
        };
    }

    public ActionListener getCloseTabListener() {
        return actionEvent -> {
            view.closeTab(model.getFileName());
        };
    }

}

